/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.kongphop.maxsubslow;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class MaxsubSlow {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputNum = sc.nextInt();
        int[] arr = new int[inputNum];
        int max = 0;

        for (int i = 0; i < inputNum; i++) {
            arr[i] = sc.nextInt();
        }
        showInput(arr);
        max = process(arr, max);
        showOutput(max);
    }

    public static void showOutput(int max) {
        System.out.println("");
        System.out.println("Output(MaxsubArray): " + max);
    }

    public static void showInput(int[] arr) {
        System.out.print("Input: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");

        }
    }

    public static int process(int[] arr, int max) {
        for (int j = 0; j < arr.length; j++) {
            for (int k = j; k < arr.length; k++) {
                int s = 0;
                for (int i = j; i <= k; i++) {
                    s = s + arr[i];
                }
                if (s > max) {
                    max = s;
                }
            }
        }
        return max;
    }
}
